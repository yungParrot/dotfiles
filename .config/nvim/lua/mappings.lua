require "nvchad.mappings"

-- add yours here

local map = vim.keymap.set

map("n", ";", ":", { desc = "CMD enter command mode" })
map("i", "jk", "<ESC>")

-- map({ "n", "i", "v" }, "<C-s>", "<cmd> w <cr>")
-- TODO reconfigure according to https://nvchad.com/docs/config/mappings
-- local M = {}

-- M.copilot = {
  -- i = {
    -- TODO: think about possible bindings
    -- ["<C-Tab>"] = {
      -- function()
        -- vim.fn.feedkeys(vim.fn['copilot#Accept'](), '')
      -- end,
      -- "Copilot Accept",
      -- {
        -- replace_keycodes = true,
        -- nowait=true,
        -- silent=true,
        -- expr=true,
        -- noremap=true,
      -- },
    -- },
  -- },
-- }

-- return M
