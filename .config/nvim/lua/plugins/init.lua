return {
  {
    "vimwiki/vimwiki",
    init = function()
      vim.g.vimwiki_list = {
        {
          path = "~/vimwiki/",
          syntax = "markdown",
          ext = ".md",
        },
      }
      vim.g.vimwiki_ext2syntax = {
        [".md"] = "markdown",
        [".markdown"] = "markdown",
        [".mdown"] = "markdown",
      }
    end,
    lazy = false,
  },
  {
    "goolord/alpha-nvim",
    disable = false,
  },
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        -- Lua
        "lua-language-server",
        "stylua",

        -- Python
        -- TODO configure LSP
        -- "flake8",
        "python-lsp-server",
        -- "black",
        "pylint",

        -- JS
        "eslint-lsp",
        "eslint_d",
        "typescript-language-server",
        "vue-language-server",

        -- Shell
        "bash-language-server",
        "shellcheck",

        -- Docker
        "dockerfile-language-server",

        -- Markdown, YAML, etc.
        "markdownlint",
        "yamllint",

        -- Go
        "gopls",
        "golangci-lint",
        "gotests",
        "go-debug-adapter",

        -- Terraform
        "terraform-ls",
        "tflint",
      },
    },
  },
  {
    "neovim/nvim-lspconfig",
    config = function()
      require "configs.lspconfig"
    end,
  },
  {
    "folke/which-key.nvim",
    disable = false,
  },
  { "mattn/calendar-vim" },
  {
    "iamcco/markdown-preview.nvim",
    run = function()
      vim.fn["mkdp#util#install"]()
    end,
  },
  {
    "weilbith/nvim-code-action-menu",
    cmd = "CodeActionMenu",
  },
  { "mfussenegger/nvim-dap" },
  {
    "mfussenegger/nvim-dap-python",
    config = function()
      require("dap-python").setup "~/.virtualenvs/debugpy/bin/python"
      require("dap-python").test_runner = "pytest"
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        -- defaults
        "vim",
        "lua",

        --Python
        "python",

        -- JS
        "html",
        "css",
        "javascript",
        "typescript",
        "json",

        -- Markdown, YAML, etc.
        "markdown",
        "yaml",

        -- TF
        "terraform",
      },
    },
  },
  {
    "github/copilot.vim",
    lazy = false,
    config = function()
      -- Mapping tab is already used by NvChad
      vim.g.copilot_no_tab_map = true
      vim.g.copilot_assume_mapped = true
      vim.g.copilot_tab_fallback = ""
      -- The mapping is set to other key, see custom/lua/mappings
      -- or run <leader>ch to see copilot mapping section
    end,
  },
  {
    "stevearc/conform.nvim",
    event = "BufWritePre", -- uncomment for format on save
    config = function()
      require "configs.conform"
    end,
  },
}
