#!/bin/sh


# Setting the wallpaper and compositor
xwallpaper --zoom ~/.config/wallpaper.jpg & disown
picom -b & disown # --experimental-backends --vsync should prevent screen tearing on most setups if needed

# Low battery notifier
~/.config/qtile/scripts/check_battery.sh & disown

# Start welcome
eos-welcome & disown

# Start polkit agent from GNOME
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & disown

# Touchegg client
touchegg & disown

# Applets
nm-applet & disown
megasync & disown

# Touchpad Natural Scrolling
xinput --set-prop "ELAN1300:00 04F3:3059 Touchpad" "libinput Natural Scrolling Enabled" 1 & disown
