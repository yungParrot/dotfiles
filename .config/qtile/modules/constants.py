from enum import Enum

import os

# Key definition
SUPER = "mod4"
ALT = "mod1"
SHIFT = "shift"
CTRL = "control"
TERMINAL = "alacritty"
HOME = os.path.expanduser("~")


class Color(Enum):
    """
    Dracula color scheme
    copied from [here](https://draculatheme.com/contribute).
    """

    BG = "#282a36"
    CURRENT = "#44475a"
    FG = "#f8f8f2"
    COMMENT = "#6272a4"
    CYAN = "#8be9fd"
    GREEN = "#50fa7b"
    ORANGE = "#ffb86c"
    PINK = "#ff79c6"
    PURPLE = "#bd93f9"
    RED = "#ff5555"
    YELLOW = "#f1fa8c"


LAYOUT_THEME = {
    "margin": 5,
    "border_width": 5,
    "border_focus": Color.PURPLE.value,
    "border_normal": Color.BG.value,
}

BAR_SIZE = 30

WALLPAPER_PATH = "~/.config/wallpaper.jpg"
WALLAPER_MODE = "fill"
