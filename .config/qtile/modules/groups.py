from libqtile.config import Key, Group
from libqtile.lazy import lazy
from .keys import keys
from .constants import SUPER, SHIFT


groups = [Group(i) for i in "123456789"]

for group in groups:
    keys.extend(
        [
            # Switching between groups
            Key(
                [SUPER],
                group.name,
                lazy.group[group.name].toscreen(),
                desc=f"Switch to group {group.name}",
            ),
            Key(
                [SUPER],
                "Right",
                lazy.screen.next_group(),
                desc="Switch to next group",
            ),
            Key(
                [SUPER],
                "Left",
                lazy.screen.prev_group(),
                desc="Switch to previous group",
            ),
            # Move focused windows to group
            Key(
                [SUPER, SHIFT],
                group.name,
                lazy.window.togroup(group.name, switch_group=True),
                desc=f"Switch to & move focused window to group {group.name}",
            ),
        ]
    )
