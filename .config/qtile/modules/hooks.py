from libqtile import hook
import subprocess
import os


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.call([home])


@hook.subscribe.client_new
def client_new(client):
    # TODO: some tweaking will be needed to make it work correctly 100%
    messaging_apps = {
        "caprine",
        "signal",
        "slack",
        "discord",
    }
    if client.name in messaging_apps:
        client.togroup("6")

    settings_apps = {
        "pavucontrol",
        "blueman-manager",
    }
    if client.name in settings_apps:
        client.togroup(9)
