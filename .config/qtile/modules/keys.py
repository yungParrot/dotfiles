import os
from libqtile.lazy import lazy
from libqtile.config import Key
from .constants import SUPER, TERMINAL, SHIFT, CTRL, ALT


def _switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


def _spawn_terminal() -> str:
    """Wrapper function for spawning terminal in a specific way"""
    command = TERMINAL
    if TERMINAL == "alacritty":
        # Will try to launch a new window or will launch a new instance
        # TODO: the command below won't work 🤔 has to be fixed...
        # command = f"{TERMINAL} msg create-window || {TERMINAL}"
        command = TERMINAL
    return command


_TERMINAL: str = _spawn_terminal()

keys = [
    # Launch terminal, kill window, restart and exit Qtile
    Key(
        [SUPER],
        "Return",
        lazy.spawn(_TERMINAL),
        desc="Launch terminal",
    ),
    Key([SUPER], "w", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [SUPER],
        "Escape",
        lazy.spawn("xkill"),
        desc="Kill window with mouse selection",
    ),
    Key([SUPER, CTRL], "r", lazy.restart(), desc="Restart Qtile"),
    Key([SUPER, CTRL], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key(
        [SUPER, SHIFT],
        "r",
        lazy.spawncmd(),
        desc="Spawn a command using a prompt widget",
    ),
    # Lock screen
    Key([SUPER, ALT], "l", lazy.spawn("dm-tool lock")),
    # Rofi app launcher
    Key(
        [SUPER],
        "r",
        lazy.spawn("rofi -modi drun -show drun -show-icons"),
        desc="Spawn rofi",
    ),
    Key(
        [SUPER],
        "e",
        lazy.spawn("rofi -show emoji -modi emoji"),
        desc="Spawn rofi-emoji",
    ),
    Key(
        [SUPER],
        "p",
        lazy.spawn(os.path.expanduser("~/.config/rofi/powermenu.sh")),
        desc="Spawn rofi-powermenu",
    ),
    # App shortcuts
    Key([SUPER, ALT], "w", lazy.spawn("brave")),
    Key([SUPER, ALT], "s", lazy.spawn("flameshot gui")),
    # Volume keys
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer set Master 3%+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer set Master 3%-")),
    Key([], "XF86AudioMute", lazy.spawn("amixer set Master toggle")),
    # Backlight keys
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight +15")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -15")),
    # Toggle layouts
    Key([SUPER], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([SUPER], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key(
        [SUPER, SHIFT],
        "space",
        lazy.window.toggle_floating(),
        desc="Toggle floating layout for selected window",
    ),
    # Switch focus between physical monitors
    Key([SUPER], "period", lazy.next_screen(), desc="Move to next screen"),
    Key([SUPER], "comma", lazy.prev_screen(), desc="Move to previous screen"),
    Key([SUPER], "t", lazy.function(_switch_screens), desc="Swap screens"),
    # Switch between windows
    Key([SUPER], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([SUPER], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([SUPER], "j", lazy.layout.down(), desc="Move focus down"),
    Key([SUPER], "k", lazy.layout.up(), desc="Move focus up"),
    Key(
        [SUPER],
        "Tab",
        lazy.layout.next(),
        desc="Move window focus to other window",
    ),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key(
        [SUPER, SHIFT],
        "h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left",
    ),
    Key(
        [SUPER, SHIFT],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key([SUPER, SHIFT], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([SUPER, SHIFT], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    # Resize layout
    Key(
        [SUPER, "control"],
        "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        desc="Grow window to the right",
    ),
    Key(
        [SUPER, "control"],
        "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        desc="Grow window to the left",
    ),
    Key(
        [SUPER, "control"],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        desc="Grow window up",
    ),
    Key(
        [SUPER, "control"],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        desc="Grow window down",
    ),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [SUPER, SHIFT],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    # Toggle between different layouts as defined below
    Key([SUPER, SHIFT, CTRL], "h", lazy.layout.swap_column_left()),
    Key([SUPER, SHIFT, CTRL], "l", lazy.layout.swap_column_right()),
    Key([SUPER, SHIFT], "space", lazy.layout.flip()),
]
