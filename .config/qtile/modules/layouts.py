from libqtile import layout
from libqtile.config import Match
from .constants import LAYOUT_THEME

layouts = [
    layout.MonadTall(**LAYOUT_THEME),
    layout.Columns(**LAYOUT_THEME),
    layout.Max(**LAYOUT_THEME),
    layout.Stack(num_stacks=2, **LAYOUT_THEME),
    layout.MonadWide(**LAYOUT_THEME),
    layout.Floating(**LAYOUT_THEME),
]

floating_layout = layout.Floating(
    floating_layout=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(title="branchdialog"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="pinentry-gtk-2"),  # GPG key password entry
        Match(wm_class="megasync"),
        Match(title="MEGAsync"),
    ],
    **LAYOUT_THEME
)
