from libqtile.config import Click, Drag
from libqtile.lazy import lazy
from .constants import SUPER

# Drag floating layouts.
mouse = [
    Drag(
        [SUPER],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [SUPER],
        "Button3",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size(),
    ),
    Click([SUPER], "Button2", lazy.window.bring_to_front()),
]
