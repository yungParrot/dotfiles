from .widgets import volume
from libqtile import bar, widget, qtile
from libqtile.lazy import lazy
from libqtile.config import Screen, Key
import os
from .constants import Color, BAR_SIZE, WALLPAPER_PATH, WALLAPER_MODE, CTRL, SUPER
from .keys import keys


screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(linewidth=0, background=Color.BG.value),
                widget.Image(
                    filename="~/.config/qtile/logo.png",
                    background=Color.BG.value,
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn("rofi -show combi")
                    },
                ),
                widget.Sep(padding=4, linewidth=0, background=Color.BG.value),
                widget.GroupBox(
                    highlight_method="line",
                    this_screen_border=Color.PURPLE.value,
                    this_current_screen_border=Color.PURPLE.value,
                    active=Color.FG.value,
                    inactive=Color.CURRENT.value,
                    background=Color.BG.value,
                    disable_drag=True,
                ),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget.WindowName(foreground=Color.COMMENT.value),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.CurrentLayout(
                    foreground=Color.COMMENT.value,
                ),
                widget.CurrentLayoutIcon(scale=0.75),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.CheckUpdates(
                    update_interval=1800,
                    distro="Arch_yay",
                    display_format="{updates} Updates",
                    foreground=Color.FG.value,
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn(terminal + " -e yay -Syu")
                    },
                    background=Color.BG.value,
                ),
                widget.TextBox(
                    text="",
                    padding=0,
                    fontsize=BAR_SIZE,
                    foreground=Color.BG.value,
                ),
                widget.Systray(icon_size=20),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.TextBox(
                    text="",
                    padding=0,
                    fontsize=BAR_SIZE,
                    foreground=Color.BG.value,
                ),
                widget.Clock(
                    format=" %Y-%m-%d %a %I:%M %p",
                    foreground=Color.GREEN.value,
                ),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.TextBox(
                    text="CPU",
                    background=Color.BG.value,
                ),
                widget.CPUGraph(
                    update_interval=1,
                    background=Color.BG.value,
                ),
                widget.TextBox(
                    text="RAM",
                    background=Color.BG.value,
                ),
                widget.MemoryGraph(
                    text="RAM",
                    background=Color.BG.value,
                ),
                widget.TextBox(
                    text="",
                    padding=0,
                    fontsize=28,
                    foreground=Color.BG.value,
                ),
                volume,
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.Backlight(
                    backlight_name="intel_backlight",
                    format="☀ {percent:2.0%}",
                    background=Color.BG.value,
                    foreground=Color.ORANGE.value,
                ),
                widget.TextBox(
                    text="",
                    padding=0,
                    fontsize=28,
                    foreground=Color.BG.value,
                ),
                widget.Battery(
                    format="🔋 {percent:2.0%} {hour:d}:{min:02d}",
                    update_interval=1,
                    foreground=Color.ORANGE.value,
                ),
                widget.TextBox(
                    text="",
                    padding=0,
                    fontsize=BAR_SIZE,
                    foreground=Color.BG.value,
                ),
                widget.TextBox(
                    text="",
                    fontsize=BAR_SIZE / 2,
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn(
                            os.path.expanduser("~/.config/rofi/powermenu.sh")
                        )
                    },
                    background=Color.BG.value,
                    foreground=Color.ORANGE.value,
                ),
                widget.Spacer(
                    length=10,
                    background=Color.BG.value,
                ),
            ],
            BAR_SIZE,
            background=Color.CURRENT.value,
        ),
        wallpaper=WALLPAPER_PATH,
        wallpaper_mode=WALLAPER_MODE,
    ),
    Screen(
        top=bar.Bar(
            widgets=[
                widget.Sep(linewidth=0, background=Color.BG.value),
                widget.Image(
                    filename="~/.config/qtile/logo.png",
                    background=Color.BG.value,
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn("rofi -show combi")
                    },
                ),
                widget.Sep(padding=4, linewidth=0, background=Color.BG.value),
                widget.GroupBox(
                    highlight_method="line",
                    this_screen_border=Color.PURPLE.value,
                    this_current_screen_border=Color.PURPLE.value,
                    active=Color.FG.value,
                    inactive=Color.CURRENT.value,
                    background=Color.BG.value,
                    disable_drag=True,
                ),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget.WindowName(foreground=Color.COMMENT.value),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.CurrentLayout(
                    foreground=Color.COMMENT.value,
                ),
                widget.CurrentLayoutIcon(scale=0.75),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
            ],
            size=BAR_SIZE,
            background=Color.CURRENT.value,
        ),
        wallpaper=WALLPAPER_PATH,
        wallpaper_mode=WALLAPER_MODE,
    ),
    Screen(
        top=bar.Bar(
            widgets=[
                widget.Sep(linewidth=0, background=Color.BG.value),
                widget.Image(
                    filename="~/.config/qtile/logo.png",
                    background=Color.BG.value,
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn("rofi -show combi")
                    },
                ),
                widget.Sep(padding=4, linewidth=0, background=Color.BG.value),
                widget.GroupBox(
                    highlight_method="line",
                    this_screen_border=Color.PURPLE.value,
                    this_current_screen_border=Color.PURPLE.value,
                    active=Color.FG.value,
                    inactive=Color.CURRENT.value,
                    background=Color.BG.value,
                    disable_drag=True,
                ),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget.WindowName(foreground=Color.COMMENT.value),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.CurrentLayout(
                    foreground=Color.COMMENT.value,
                ),
                widget.CurrentLayoutIcon(scale=0.75),
                widget.TextBox(
                    text="", padding=0, fontsize=BAR_SIZE, foreground=Color.BG.value
                ),
            ],
            size=BAR_SIZE,
            background=Color.CURRENT.value,
        ),
        wallpaper=WALLPAPER_PATH,
        wallpaper_mode=WALLAPER_MODE,
    ),
]


for index, screen in enumerate(screens):
    keys.extend(
        [
            Key(
                [SUPER, CTRL],
                str(index + 1),
                lazy.to_screen(index),
                desc="Move focus to specific screen",
            ),
        ]
    )
