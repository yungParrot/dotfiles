#!/bin/sh

ICON="󰡨"

DOCKER_PS=$(ps -ef | grep "Docker.app" | wc -l)
STATUS=$([[ ${DOCKER_PS} -gt 1 ]] && echo "Running" || echo "Not running")

# The item invoking this script (name $NAME) will get its icon and label
# updated with the current battery status
sketchybar --set $NAME icon="$ICON" label="${STATUS}"
