#!/bin/sh

# The wifi_change event supplies a $INFO variable in which the current SSID
# is passed to the script.

INFO=$( \
  ipconfig getsummary \
  $(networksetup -listallhardwareports | awk '/Hardware Port: Wi-Fi/{getline; print $2}') \
  | awk -F ' SSID : ' '/ SSID : / {print $2}'\
)
WIFI=${INFO:-"Not Connected"}

sketchybar --set $NAME label="${WIFI}"
