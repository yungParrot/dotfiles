# ssh-agent
if [ -z ${XDG_RUNTIME_DIR+x} ]; then
  # XDG_RUNTIME_DIR isn't set by default on macOS
  # so I have to create some sort of runtime directory
  mkdir -p $HOME/.run
  XDG_RUNTIME_DIR=$HOME/.run
fi
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# Qtile + Xorg
# TODO: I'll have to check if this is still in use
if [ "$(tty)" = "/dev/tty1" ]; then
    pgrep qtile || exec startx "$XDG_CONFIG_HOME/X11/xinitrc" 
fi

