typeset -U PATH path


export XDG_RUNTIME_DIR="/run/user/$UID"

# Exporting custom XDG paths
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# XDG Base Directories
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"

# Disabling history for less
export LESSHISTFILE=-

# Changing ZSH config path
export ZDOTDIR=$XDG_CONFIG_HOME/zsh

# Fixing dotfiles in home directory
# docker
export DOCKER_CONFIG=$XDG_CONFIG_HOME/docker
# gnupg
export GNUPGHOME=$XDG_DATA_HOME/gnupg
# gtk-2
export GTK2_RC_FILES=$XDG_CONFIG_HOME/gtk-2.0/gtkrc
# ipython
export IPYTHONDIR=$XDG_CONFIG_HOME/ipython
# python history
export PYTHONSTARTUP="${XDG_CONFIG_HOME}/python/pythonrc"
# python poetry keyring fix
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
# jupyter
export JUPYTER_CONFIG_DIR=$XDG_CONFIG_HOME/jupyter
# less
export LESSHISTFILE=$XDG_CACHE_HOME/less/history
# nodejs
export NODE_REPL_HISTORY=$XDG_DATA_HOME/node_repl_history
# pyenv
export PYENV_ROOT=$XDG_DATA_HOME/pyenv
# vagrant
export VAGRANT_HOME=$XDG_DATA_HOME/vagrant
# wget
alias wget="wget --hsts-file=$XDG_DATA_HOME/wget-hsts"
# android-studio
# for some reasone it's not working 🤔
export ANDROID_HOME="$XDG_DATA_HOME"/android
export _JAVA_AWT_WM_NONREPARENTING=1
# java
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
# gradle
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
# sqlite
export SQLITE_HISTORY="$XDG_CACHE_HOME"/sqlite_history
# npm
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
# nvm
export NVM_DIR="$XDG_DATA_HOME"/nvm
# golang
export GOPATH=$HOME/projects/go
# aws
export AWS_SHARED_CREDENTIALS_FILE="$XDG_CONFIG_HOME"/aws/credentials
export AWS_CONFIG_FILE="$XDG_CONFIG_HOME"/aws/config
# pass
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass
# azure
export AZURE_CONFIG_DIR="$XDG_DATA_HOME"/azure
# terminfo
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo
