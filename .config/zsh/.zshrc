# !/usr/bin/env bash
## Plugins (Antidote)
# This is required for compdef etc.
autoload -Uz compinit
compinit

# For running 2 installations of Homebrew
# https://stackoverflow.com/a/64951025
brew_prefix=$(brew --prefix) &> /dev/null
[[ -z ${brew_prefix} || ${brew_prefix} == "/usr/local" ]] && brew_prefix="/opt/homebrew"
alias brow="arch --x86_64 /usr/local/Homebrew/bin/brew"
alias brew="${brew_prefix}/bin/brew"
source ${brew_prefix}/opt/antidote/share/antidote/antidote.zsh
# initialize plugins statically with ${ZDOTDIR:-~}/.zsh_plugins.txt
antidote load

# Binding Ctrl+Arrow Keys
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^A" beginning-of-line 
bindkey "^E" end-of-line
# Binding Delete Key
bindkey "^[[3~" delete-char
bindkey "^[3;5~" delete-char

## DotFiles
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'

## VimWiki
alias wiki='/usr/bin/git --git-dir=$HOME/vimwiki/.git --work-tree=$HOME/vimwiki/'

## History Command - from OhMyZsh
# History file
export HISTFILE=$HOME/.cache/.zsh_history
# History wrapper
function omz_history {
  local clear list
  zparseopts -E c=clear l=list

  if [[ -n "$clear" ]]; then
    # if -c provided, clobber the history file
    echo -n >| "$HISTFILE"
    fc -p "$HISTFILE"
    echo >&2 History file deleted.
  elif [[ -n "$list" ]]; then
    # if -l provided, run as if calling `fc' directly
    builtin fc "$@"
  else
    # unless a number is provided, show all history events (starting from 1)
    [[ ${@[-1]-} = *[0-9]* ]] && builtin fc -l "$@" || builtin fc -l "$@" 1
  fi
}

# Timestamp format
case ${HIST_STAMPS-} in
  "mm/dd/yyyy") alias history='omz_history -f' ;;
  "dd.mm.yyyy") alias history='omz_history -E' ;;
  "yyyy-mm-dd") alias history='omz_history -i' ;;
  "") alias history='omz_history' ;;
  *) alias history="omz_history -t '$HIST_STAMPS'" ;;
esac

# History file configuration
[ -z "$HISTFILE" ] && HISTFILE="$HOME/.zsh_history"
[ "$HISTSIZE" -lt 50000 ] && HISTSIZE=50000
[ "$SAVEHIST" -lt 10000 ] && SAVEHIST=10000

# History command configuration
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt share_history          # share command history data

## ls aliases
alias ls="ls --color=auto"
alias ll="ls -lh"
alias la="ls -lah"

# 
ZSH_DISABLE_COMPFIX="true"

# PATH exports
# Exporting custom script path in ~/bin
PATH=$PATH:$HOME/bin
# MacOS Homebrew export
PATH=$PATH:/opt/homebrew/bin
# Yarn global export
command -v yarn > /dev/null && \
  PATH="$PATH:$(yarn global bin)" 
# Node version management
export NVM_DIR="$HOME/.nvm"
# This loads nvm
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"
# This loads nvm bash_completion
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"
# Scripts from ~/.local/bin 
PATH=$PATH:$HOME/.local/bin
export PATH
# pinentry-tty
export GPG_TTY="$( tty )"

# Bash style autocompletion
setopt noautomenu
setopt nomenucomplete

# Pyenv export
# PYENV_ROOT is set in zshenv
export PATH="$PYENV_ROOT/bin:$PATH"
command -v pyenv > /dev/null && \
  eval "$(pyenv init -)"

# Poetry export
export PATH="$HOME/.poetry/bin:$PATH"

# kubectl completion
command -v kubectl > /dev/null && \
  source <(kubectl completion zsh)
alias kc="kubectl"

# Has to be last
# For some reason it wasn't working on Arch when installed through ZSH plugins
ZSH_SYNTAX_HL_PATH="/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
[[ -f ${ZSH_SYNTAX_HL_PATH} ]] && source ${ZSH_SYNTAX_HL_PATH}
