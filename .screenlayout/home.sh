#!/bin/sh
# Home xrandr setup with laptop 💻 on under a ultrawide display 📺
# with one other other display on the left ⬅

xrandr \
  --output eDP1 \
  --primary \
  --mode 1920x1080 \
  -r 60 \
  --pos 2680x1440 \
  --rotate normal \
  --output DP1 \
  --off \
  --output HDMI1 \
  --off \
  --output HDMI2 \
  --off \
  --output VIRTUAL1 \
  --off \
  --output DVI-I-1-1 \
  --mode 3440x1440 \
  -r 144 \
  --pos 1920x0 \
  --rotate normal \
  --output DVI-I-2-2 \
  --mode 1920x1080 \
  -r 60 \
  --pos 0x0 \
  --rotate normal
