#!/bin/sh
# Laptop xrandr setup with only the internal display 💻

xrandr \
  --output eDP1 \
  --primary \
  --mode 1920x1080 \
  -r 60 \
  --rotate normal \
  --output DP1 \
  --off \
  --output HDMI1 \
  --off \
  --output HDMI2 \
  --off \
  --output VIRTUAL1 \
  --off \
  --output DVI-I-1-1 \
  --off \
  --off \
  --output DVI-I-2-2 \
  --off
